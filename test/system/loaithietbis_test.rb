require "application_system_test_case"

class LoaithietbisTest < ApplicationSystemTestCase
  setup do
    @loaithietbi = loaithietbis(:one)
  end

  test "visiting the index" do
    visit loaithietbis_url
    assert_selector "h1", text: "Loaithietbis"
  end

  test "creating a Loaithietbi" do
    visit loaithietbis_url
    click_on "New Loaithietbi"

    fill_in "Donvitinh", with: @loaithietbi.donvitinh
    fill_in "Ghichu", with: @loaithietbi.ghichu
    fill_in "Tenloai", with: @loaithietbi.tenloai
    click_on "Create Loaithietbi"

    assert_text "Loaithietbi was successfully created"
    click_on "Back"
  end

  test "updating a Loaithietbi" do
    visit loaithietbis_url
    click_on "Edit", match: :first

    fill_in "Donvitinh", with: @loaithietbi.donvitinh
    fill_in "Ghichu", with: @loaithietbi.ghichu
    fill_in "Tenloai", with: @loaithietbi.tenloai
    click_on "Update Loaithietbi"

    assert_text "Loaithietbi was successfully updated"
    click_on "Back"
  end

  test "destroying a Loaithietbi" do
    visit loaithietbis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Loaithietbi was successfully destroyed"
  end
end
