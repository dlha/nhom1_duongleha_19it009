require "application_system_test_case"

class NhasanxuatsTest < ApplicationSystemTestCase
  setup do
    @nhasanxuat = nhasanxuats(:one)
  end

  test "visiting the index" do
    visit nhasanxuats_url
    assert_selector "h1", text: "Nhasanxuats"
  end

  test "creating a Nhasanxuat" do
    visit nhasanxuats_url
    click_on "New Nhasanxuat"

    fill_in "Quocgia", with: @nhasanxuat.quocgia
    fill_in "Ten", with: @nhasanxuat.ten
    click_on "Create Nhasanxuat"

    assert_text "Nhasanxuat was successfully created"
    click_on "Back"
  end

  test "updating a Nhasanxuat" do
    visit nhasanxuats_url
    click_on "Edit", match: :first

    fill_in "Quocgia", with: @nhasanxuat.quocgia
    fill_in "Ten", with: @nhasanxuat.ten
    click_on "Update Nhasanxuat"

    assert_text "Nhasanxuat was successfully updated"
    click_on "Back"
  end

  test "destroying a Nhasanxuat" do
    visit nhasanxuats_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Nhasanxuat was successfully destroyed"
  end
end
