require "application_system_test_case"

class ThietbisTest < ApplicationSystemTestCase
  setup do
    @thietbi = thietbis(:one)
  end

  test "visiting the index" do
    visit thietbis_url
    assert_selector "h1", text: "Thietbis"
  end

  test "creating a Thietbi" do
    visit thietbis_url
    click_on "New Thietbi"

    fill_in "Loaithietbi", with: @thietbi.loaithietbi_id
    fill_in "Nhacungcap", with: @thietbi.nhacungcap_id
    fill_in "Nhasanxuat", with: @thietbi.nhasanxuat_id
    fill_in "Tenthietbi", with: @thietbi.tenthietbi
    fill_in "Thongsokt", with: @thietbi.thongsokt
    click_on "Create Thietbi"

    assert_text "Thietbi was successfully created"
    click_on "Back"
  end

  test "updating a Thietbi" do
    visit thietbis_url
    click_on "Edit", match: :first

    fill_in "Loaithietbi", with: @thietbi.loaithietbi_id
    fill_in "Nhacungcap", with: @thietbi.nhacungcap_id
    fill_in "Nhasanxuat", with: @thietbi.nhasanxuat_id
    fill_in "Tenthietbi", with: @thietbi.tenthietbi
    fill_in "Thongsokt", with: @thietbi.thongsokt
    click_on "Update Thietbi"

    assert_text "Thietbi was successfully updated"
    click_on "Back"
  end

  test "destroying a Thietbi" do
    visit thietbis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Thietbi was successfully destroyed"
  end
end
