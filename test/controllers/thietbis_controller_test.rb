require "test_helper"

class ThietbisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @thietbi = thietbis(:one)
  end

  test "should get index" do
    get thietbis_url
    assert_response :success
  end

  test "should get new" do
    get new_thietbi_url
    assert_response :success
  end

  test "should create thietbi" do
    assert_difference('Thietbi.count') do
      post thietbis_url, params: { thietbi: { loaithietbi_id: @thietbi.loaithietbi_id, nhacungcap_id: @thietbi.nhacungcap_id, nhasanxuat_id: @thietbi.nhasanxuat_id, tenthietbi: @thietbi.tenthietbi, thongsokt: @thietbi.thongsokt } }
    end

    assert_redirected_to thietbi_url(Thietbi.last)
  end

  test "should show thietbi" do
    get thietbi_url(@thietbi)
    assert_response :success
  end

  test "should get edit" do
    get edit_thietbi_url(@thietbi)
    assert_response :success
  end

  test "should update thietbi" do
    patch thietbi_url(@thietbi), params: { thietbi: { loaithietbi_id: @thietbi.loaithietbi_id, nhacungcap_id: @thietbi.nhacungcap_id, nhasanxuat_id: @thietbi.nhasanxuat_id, tenthietbi: @thietbi.tenthietbi, thongsokt: @thietbi.thongsokt } }
    assert_redirected_to thietbi_url(@thietbi)
  end

  test "should destroy thietbi" do
    assert_difference('Thietbi.count', -1) do
      delete thietbi_url(@thietbi)
    end

    assert_redirected_to thietbis_url
  end
end
