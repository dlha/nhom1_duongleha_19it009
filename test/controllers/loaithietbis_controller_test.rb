require "test_helper"

class LoaithietbisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @loaithietbi = loaithietbis(:one)
  end

  test "should get index" do
    get loaithietbis_url
    assert_response :success
  end

  test "should get new" do
    get new_loaithietbi_url
    assert_response :success
  end

  test "should create loaithietbi" do
    assert_difference('Loaithietbi.count') do
      post loaithietbis_url, params: { loaithietbi: { donvitinh: @loaithietbi.donvitinh, ghichu: @loaithietbi.ghichu, tenloai: @loaithietbi.tenloai } }
    end

    assert_redirected_to loaithietbi_url(Loaithietbi.last)
  end

  test "should show loaithietbi" do
    get loaithietbi_url(@loaithietbi)
    assert_response :success
  end

  test "should get edit" do
    get edit_loaithietbi_url(@loaithietbi)
    assert_response :success
  end

  test "should update loaithietbi" do
    patch loaithietbi_url(@loaithietbi), params: { loaithietbi: { donvitinh: @loaithietbi.donvitinh, ghichu: @loaithietbi.ghichu, tenloai: @loaithietbi.tenloai } }
    assert_redirected_to loaithietbi_url(@loaithietbi)
  end

  test "should destroy loaithietbi" do
    assert_difference('Loaithietbi.count', -1) do
      delete loaithietbi_url(@loaithietbi)
    end

    assert_redirected_to loaithietbis_url
  end
end
