require "test_helper"

class NhasanxuatsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nhasanxuat = nhasanxuats(:one)
  end

  test "should get index" do
    get nhasanxuats_url
    assert_response :success
  end

  test "should get new" do
    get new_nhasanxuat_url
    assert_response :success
  end

  test "should create nhasanxuat" do
    assert_difference('Nhasanxuat.count') do
      post nhasanxuats_url, params: { nhasanxuat: { quocgia: @nhasanxuat.quocgia, ten: @nhasanxuat.ten } }
    end

    assert_redirected_to nhasanxuat_url(Nhasanxuat.last)
  end

  test "should show nhasanxuat" do
    get nhasanxuat_url(@nhasanxuat)
    assert_response :success
  end

  test "should get edit" do
    get edit_nhasanxuat_url(@nhasanxuat)
    assert_response :success
  end

  test "should update nhasanxuat" do
    patch nhasanxuat_url(@nhasanxuat), params: { nhasanxuat: { quocgia: @nhasanxuat.quocgia, ten: @nhasanxuat.ten } }
    assert_redirected_to nhasanxuat_url(@nhasanxuat)
  end

  test "should destroy nhasanxuat" do
    assert_difference('Nhasanxuat.count', -1) do
      delete nhasanxuat_url(@nhasanxuat)
    end

    assert_redirected_to nhasanxuats_url
  end
end
