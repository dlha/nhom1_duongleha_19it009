class ThietbisController < ApplicationController
  before_action :set_thietbi, only: %i[ show edit update destroy ]

  # GET /thietbis or /thietbis.json
  def index
    @thietbis = Thietbi.all
  end

  # GET /thietbis/1 or /thietbis/1.json
  def show
  end

  # GET /thietbis/new
  def new
    @thietbi = Thietbi.new
  end

  # GET /thietbis/1/edit
  def edit
  end

  # POST /thietbis or /thietbis.json
  def create
    @thietbi = Thietbi.new(thietbi_params)

    respond_to do |format|
      if @thietbi.save
        format.html { redirect_to @thietbi, notice: "Thietbi was successfully created." }
        format.json { render :show, status: :created, location: @thietbi }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @thietbi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /thietbis/1 or /thietbis/1.json
  def update
    respond_to do |format|
      if @thietbi.update(thietbi_params)
        format.html { redirect_to @thietbi, notice: "Thietbi was successfully updated." }
        format.json { render :show, status: :ok, location: @thietbi }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @thietbi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /thietbis/1 or /thietbis/1.json
  def destroy
    @thietbi.destroy
    respond_to do |format|
      format.html { redirect_to thietbis_url, notice: "Thietbi was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_thietbi
      @thietbi = Thietbi.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def thietbi_params
      params.require(:thietbi).permit(:tenthietbi, :nhasanxuat_id, :thongsokt, :loaithietbi_id, :nhacungcap_id)
    end
end
