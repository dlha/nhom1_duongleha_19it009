class NhasanxuatsController < ApplicationController
  before_action :set_nhasanxuat, only: %i[ show edit update destroy ]

  # GET /nhasanxuats or /nhasanxuats.json
  def index
    @nhasanxuats = Nhasanxuat.all
  end

  # GET /nhasanxuats/1 or /nhasanxuats/1.json
  def show
  end

  # GET /nhasanxuats/new
  def new
    @nhasanxuat = Nhasanxuat.new
  end

  # GET /nhasanxuats/1/edit
  def edit
  end

  # POST /nhasanxuats or /nhasanxuats.json
  def create
    @nhasanxuat = Nhasanxuat.new(nhasanxuat_params)

    respond_to do |format|
      if @nhasanxuat.save
        format.html { redirect_to @nhasanxuat, notice: "Nhasanxuat was successfully created." }
        format.json { render :show, status: :created, location: @nhasanxuat }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nhasanxuat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nhasanxuats/1 or /nhasanxuats/1.json
  def update
    respond_to do |format|
      if @nhasanxuat.update(nhasanxuat_params)
        format.html { redirect_to @nhasanxuat, notice: "Nhasanxuat was successfully updated." }
        format.json { render :show, status: :ok, location: @nhasanxuat }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nhasanxuat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nhasanxuats/1 or /nhasanxuats/1.json
  def destroy
    @nhasanxuat.destroy
    respond_to do |format|
      format.html { redirect_to nhasanxuats_url, notice: "Nhasanxuat was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nhasanxuat
      @nhasanxuat = Nhasanxuat.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def nhasanxuat_params
      params.require(:nhasanxuat).permit(:ten, :quocgia)
    end
end
