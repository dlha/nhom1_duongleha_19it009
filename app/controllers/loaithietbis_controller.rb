class LoaithietbisController < ApplicationController
  before_action :set_loaithietbi, only: %i[ show edit update destroy ]

  # GET /loaithietbis or /loaithietbis.json
  def index
    @loaithietbis = Loaithietbi.all
  end

  # GET /loaithietbis/1 or /loaithietbis/1.json
  def show
  end

  # GET /loaithietbis/new
  def new
    @loaithietbi = Loaithietbi.new
  end

  # GET /loaithietbis/1/edit
  def edit
  end

  # POST /loaithietbis or /loaithietbis.json
  def create
    @loaithietbi = Loaithietbi.new(loaithietbi_params)

    respond_to do |format|
      if @loaithietbi.save
        format.html { redirect_to @loaithietbi, notice: "Loaithietbi was successfully created." }
        format.json { render :show, status: :created, location: @loaithietbi }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @loaithietbi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loaithietbis/1 or /loaithietbis/1.json
  def update
    respond_to do |format|
      if @loaithietbi.update(loaithietbi_params)
        format.html { redirect_to @loaithietbi, notice: "Loaithietbi was successfully updated." }
        format.json { render :show, status: :ok, location: @loaithietbi }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @loaithietbi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loaithietbis/1 or /loaithietbis/1.json
  def destroy
    @loaithietbi.destroy
    respond_to do |format|
      format.html { redirect_to loaithietbis_url, notice: "Loaithietbi was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loaithietbi
      @loaithietbi = Loaithietbi.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def loaithietbi_params
      params.require(:loaithietbi).permit(:tenloai, :donvitinh, :ghichu)
    end
end
