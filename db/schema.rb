# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_08_061451) do

  create_table "loaithietbis", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "tenloai"
    t.string "donvitinh"
    t.string "ghichu"
  end

  create_table "nhacungcaps", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "ten"
    t.string "diachi"
    t.string "sdt"
  end

  create_table "nhasanxuats", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "ten"
    t.string "quocgia"
  end

  create_table "thietbis", charset: "utf8mb4", collation: "utf8mb4_0900_ai_ci", force: :cascade do |t|
    t.string "tenthietbi"
    t.bigint "nhasanxuat_id", null: false
    t.string "thongsokt"
    t.bigint "loaithietbi_id", null: false
    t.bigint "nhacungcap_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["loaithietbi_id"], name: "index_thietbis_on_loaithietbi_id"
    t.index ["nhacungcap_id"], name: "index_thietbis_on_nhacungcap_id"
    t.index ["nhasanxuat_id"], name: "index_thietbis_on_nhasanxuat_id"
  end

  add_foreign_key "thietbis", "loaithietbis"
  add_foreign_key "thietbis", "nhacungcaps"
  add_foreign_key "thietbis", "nhasanxuats"
end
