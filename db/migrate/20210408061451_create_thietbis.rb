class CreateThietbis < ActiveRecord::Migration[6.1]
  def change
    create_table :thietbis do |t|
      t.string :tenthietbi
      t.references :nhasanxuat, null: false, foreign_key: true
      t.string :thongsokt
      t.references :loaithietbi, null: false, foreign_key: true
      t.references :nhacungcap, null: false, foreign_key: true

      t.timestamps
    end
  end
end
